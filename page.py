#: ------------------------------------------------ IMPORTS ------------------------------------------------
import json

from flask import Flask

#: ----------------------------------------------- VARIABLES -----------------------------------------------
app = Flask(__name__)
app.config['DEBUG'] = True


#: ------------------------------------------------- CLASS -------------------------------------------------
#: ------------------------------------------------ METHODS ------------------------------------------------
@app.route('/')
@app.route('/home')
def hello():
    my_dict = {
        "response": [
            {
                "id": 1,
                "severity": "error",
                "list": ["one", 1],
                "log": "This is response from Flask web app 1"
            },
            {
                "id": 2,
                "severity": "normal",
                "list": ["two", 2],
                "log": "This is response from Flask web app 2"
            }
        ]
    }

    response = app.response_class(
        response=json.dumps(my_dict),
        status=200,
        mimetype='application/json'
    )

    return response


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8080')
