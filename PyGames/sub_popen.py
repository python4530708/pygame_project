#: ------------------------------------------------ IMPORTS ------------------------------------------------
import timeit

from subprocess import Popen, PIPE

from PyGames.PyGames_functions import adv_logger
# from PyGames.PyGames_functions import advanced_logthis


#: ----------------------------------------------- VARIABLES -----------------------------------------------
# adv_logger = advanced_logthis()
#: ------------------------------------------------- CLASS -------------------------------------------------
class RaiseErrorFromCMD(Exception):
    pass


#: ------------------------------------------------ METHODS ------------------------------------------------
def main(process: str = 'notepad.exe') -> None:
    # print("sub_popen")
    proc = process.split(' ')

    # print(f'process: {str(proc)}')

    # process = Popen(proc, stdout=PIPE, stderr=PIPE)
    # stdout, stderr = process.communicate()
    # print(f'sdtout: \n{stdout.decode("utf-8")}')
    # print(f'sdterr: \n{stderr.decode("utf-8")}')

    try:
        start = timeit.default_timer()
        process = Popen(proc, stdout=PIPE, stderr=PIPE, encoding='utf-8')
        stdout, stderr = process.communicate()

        # print(f'sdtout: \n{stdout}')
        # print(f'sdterr: \n{stderr}')

        if stderr != "":
            raise RaiseErrorFromCMD(f'{stderr.strip()} | proc: {str(proc)}')

        if stdout is not None:
            print(f'--------------------------- OUTPUT ---------------------------')
            print(stdout)
            print(f'{"-" * 25} END OUTPUT {"-" * 25}')

        end = timeit.default_timer()
        my_time = round(end -start, 3)

        msg = f'This run took: {my_time} sec, successfully ended {str(proc)}'
        print(msg)
        adv_logger.info(msg)

        # python -c "import os; size = os.get_terminal_size(); print(size[0])"

        # if isinstance(stdout, str):
        #     print(True)
        # else:
        #     print(False)

    except RaiseErrorFromCMD as ex:
        msg = f'RaiseErrorFromCMD: {ex}'
        print(msg)
        adv_logger.critical(msg)


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    main()
