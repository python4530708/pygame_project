#: ------------------------------------------------ IMPORTS ------------------------------------------------
import configparser
import sqlite3

from pathlib import Path
from PyGames.setting import Settings

#: ----------------------------------------------- VARIABLES -----------------------------------------------
config_file = 'config.ini'
config_file2 = Settings()
print(config_file2)


#: ------------------------------------------------- CLASS -------------------------------------------------
class StoreLogInDB:
    def __init__(self, dbname: str, dbpath: str, column_list: list, table_name: str) -> object:
        self.db_name = dbname
        self.db_path = Path(dbpath)
        self.full_path = self.db_path / self.db_name

        #: work with DB and cursor
        self.columns = column_list
        self.table_name = table_name

        if not self.full_path.is_file():
            self.create_table()

    def connect_db(self):
        return sqlite3.connect(self.full_path)

    def create_table(self):
        con = self.connect_db()
        cur = con.cursor()

        #: if table does not exist, create table and add columns to table
        if not self.table_exist(self.table_name):
            cur.execute(f'''CREATE TABLE IF NOT EXISTS {self.table_name} (ID integer PRIMARY KEY)''')
            for col in self.columns:
                cur.execute(f'ALTER TABLE {self.table_name} ADD {col}')

            con.commit()

        con.close()

    def get_line(self) -> list:
        """
        this will return all lines from DB
        :return: list
        """
        output = []
        con = self.connect_db()
        cur = con.cursor()

        column = ", ".join(self.columns)

        cur.execute(f"""SELECT {column} FROM {self.table_name}""")
        lines = cur.fetchall()

        if lines is not None:
            for line in lines:
                line = list(line)
                line = self.transform_chars(data=line, to_db_method=False)
                output.append(line)

        con.close()

        return output

    def set_line(self, data: list) -> None:
        """
        Store line provided as list to DB
        :param data: list, line to by stored in DB
        :return: None
        """
        con = self.connect_db()
        cur = con.cursor()

        column = ", ".join(self.columns)
        data = self.transform_chars(data=data)
        for i, my_string in enumerate(data):
            data[i] = f"'{my_string}'"
        line = ", ".join(data)

        if len(self.columns) == len(data):
            cur.execute(f'''INSERT INTO {self.table_name} ({column}) VALUES ({line})''')
        else:
            print('Provided line does not have same number of columns as table')

        con.commit()
        con.close()


    def table_exist(self, table: str) -> bool:
        """
        Check if provided table (string) exists in sqlite3 DB
        :param table: str, name of table
        :return: bool
        """
        output = False

        con = self.connect_db()
        cur = con.cursor()
        cur.execute(f"""SELECT name FROM sqlite_master WHERE type='table' AND name='{table}'""")

        lines = cur.fetchone()
        if lines is not None:
            if lines[0] == table:
                output = True

        con.close()

        return output

    @staticmethod
    def transform_chars(data: list, to_db_method: bool = True) -> list:
        """
        change character to use with sqlite3 DB
        :param data: list, list of strings where you want to change "'" to "&apos" and vice versa
        :param to_db_method: bool, defaul True
        :return: list, same list, but characters were changed
        """
        if to_db_method:
            for i, my_string in enumerate(data):
                tmp = my_string.replace("'", "&apos")
                data[i] = tmp
        else:
            for i, my_string in enumerate(data):
                tmp = my_string.replace("&apos", "'")
                data[i] = tmp

        return data


# from sqlmodel import Field, SQLModel, Session, create_engine, select
# class MyTable(SQLModel, table=True):
#     id: int = Field(default=None, primary_key=True)
#     name: str
#     age: int = None





#: ------------------------------------------------ METHODS ------------------------------------------------
def load_config() -> object:
    """
    Load config file .ini
    :return: object
    """
    global config_file
    config = configparser.ConfigParser()
    config.read(config_file)
    return config


def save_config(config) -> None:
    """
    save config file from default
    :param config: configparser
    :return: None
    """
    global config_file
    with open(config_file, 'w') as configfile:
        config.write(configfile)


def main() -> None:
    """
    Run script table.py
    :return: None
    """
    global config_file

    #: This is for .ini config
    # if Path(config_file).exists():
    #     config = load_config()
    # else:
    #     config = configparser.ConfigParser()
    #     config['DEFAULT']['db_name'] = 'test.db'
    #     config['DEFAULT']['db_path'] = '/tmp/Database/'
    #     config['DEFAULT']['columns'] = 'datetime,logger,log_level,msg'
    #     config['DEFAULT']['table'] = 'log'
    #
    #     config['LOGFILE']['name'] = 'PyGame.log'
    #
    #     save_config(config)

    #: Create object DB
    # db = StoreLogInDB(
    #     dbname=config['DEFAULT']['db_name'],
    #     dbpath=config['DEFAULT']['db_path'],
    #     column_list=config['DEFAULT']['columns'].split(','),
    #     table_name=config['DEFAULT']['table']
    # )

    #: This is for Base seetings
    db = StoreLogInDB(
        dbname=config_file2.db_name,
        dbpath=config_file2.db_path,
        column_list=config_file2.columns.split(','),
        table_name=config_file2.table
    )

    #: if table do not exist, create one
    if not db.table_exist('log'):
        db.create_table()
        print(f'Creating table {db.table_name}')

    #: open logfile
    # with open(config['LOGFILE']['name'], 'r', encoding='utf-8') as file:
    with open(config_file2.log_name, 'r', encoding='utf-8') as file:
        lines = file.readlines()

    #: store lines to DB
    for line in lines:
        # data_list = line.strip().split(';', len(config['DEFAULT']['columns'].split(',')))
        data_list = line.strip().split(';', len(config_file2.columns.split(',')))
        db.set_line(data_list)

    #: read lines from DB
    for line in db.get_line():
        print(f'{", ".join(line)}')


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    main()

    #: Testujeme SQL model
    # mt = MyTable(name="Wollwo", age=35)
    # print(mt)
    #
    # engine = create_engine('sqlite:////tmp/Database/test.db')
    # SQLModel.metadata.create_all(engine)
    #
    # with Session(engine) as session:
    #     # session.add(mt)
    #     # session.commit()
    #     ja = select(MyTable)
    #     result = session.exec(ja).all()
    #
    #     session.close()
    #
    # print(result)
    # print(result[1].age)
    # result[1].age = 10
    # print(result[1].age)

