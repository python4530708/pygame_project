#: ------------------------------------------------ IMPORTS ------------------------------------------------
import re

from PyGames.PyGames_functions import adv_logger

#: ----------------------------------------------- VARIABLES -----------------------------------------------
file_name = "PyGame.log"


#: ------------------------------------------------- CLASS -------------------------------------------------
#: ------------------------------------------------ METHODS ------------------------------------------------


def main(find_word: str, replace_word: str = '') -> None:
    global file_name

    try:
        print(f'> Opening file {file_name}')
        lines = read_file()

    except FileNotFoundError as ex:
        msg = f'File not Found: {ex}'
        print(msg)
        adv_logger.error(msg)
        exit(1)
    except UnicodeDecodeError as ex:
        msg = f'Not a regular file: {ex}'
        print(msg)
        adv_logger.error(msg)
        exit(1)

    if replace_word == '':
        #: Using str.find() to look for word
        print(f'> Looking for word: {find_word} via "str.find()"')
        for i, line in enumerate(lines):
            found = line.find(find_word)
            if found > -1:
                print(f'{i:>3} - {line}', end='')

        adv_logger.info(f'Looking for word "{find_word}", found it in {i} lines [str.find()]')

        #: Using re.search() to look for word
        print(f'> Looking for word: {find_word} via "re.search()"')
        for i, line in enumerate(lines):
            found = re.search(find_word, line)
            # found = re.search('^.*Harry.*', line)
            if found is not None:
                # print(found.group())
                print(f'{i:>3} - {line}', end='')
                # print(found.groups())

        adv_logger.info(f'Looking for word "{find_word}", found it in {i} lines [re.search()]')

        # print(find_word.lower())
        # print(find_word.upper())
        # print(find_word.lower().capitalize())
        # print(find_word.split())
        # print(find_word[::-1])no?

        #: Nahrad slovo inym, "str.replace()"
    else:
        print(f'> Replacing word: {find_word} with {replace_word} via "str.replace()"')
        save = []
        for line in lines:
            if line.find(find_word) > -1:
                line = line.replace(find_word, replace_word)
                print(line, end='')
                save.append(line)

        write_file(save=save)


def read_file() -> list:
    global file_name

    with open(file_name, "r", encoding='utf-8') as file:
        lines = file.readlines()

    # file = open()
    # lines = file.readlines()
    # file.close()

    return lines


def write_file(save: list) -> None:
    global file_name

    with open(f'out_{file_name}', "w", encoding='utf-8') as file:
        for line in save:
            file.write(line)


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    main()
