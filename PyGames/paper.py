#: ------------------------------------------------ IMPORTS ------------------------------------------------
import logging
import random
import signal

from PyGames.PyGames_functions import exit_handler

#: ----------------------------------------------- VARIABLES -----------------------------------------------
win_options = {
    'Rock': {'Lizard': 'crushes', 'Scissors': 'crushes'},
    'Paper': {'Rock': 'covers', 'Spock': 'disproves'},
    'Scissors': {'Lizard': 'Decapitate', 'paper': 'cuts'},
    'Lizard': {'Paper': 'eats', 'Spock': 'poisons'},
    'Spock': {'Rock': 'vaporizes', 'Scissors': 'smashes'}
}


#: ------------------------------------------------- CLASS -------------------------------------------------
#: ------------------------------------------------ METHODS ------------------------------------------------
def check_win(option: str, test: str) -> bool:
    """

    :param option: str
    :param test: str
    :return: bool
    """
    global win_options
    if test in win_options[option].keys():
        return True
    else:
        return False


def decide_win(player: str, cpu: str) -> None:
    """

    :param player: str
    :param cpu: str
    :return: None
    """
    global win_options

    if check_win(option=player, test=cpu):
        msg = f"Player's {player} {win_options[player][cpu]} cpu's {cpu}, Player WON!"
        print(msg)
        logging.info(msg)
    elif check_win(option=cpu, test=player):
        msg = f"CPU's {cpu} {win_options[cpu][player]} player's {player}, CPU WON!"
        print(msg)
        logging.info(msg)
    else:
        msg = f"Both sides picked {player}, it is DRAW!"
        print(msg)
        logging.info(msg)


def get_cpu_option(max_range: int = 4) -> int:
    """
    return random option from global variable win_object
    :param max_range: int, default 4
    :return: int
    """
    global win_options

    option = random.randint(0, max_range)

    return list(win_options.keys())[option]


def main(player_option='') -> None:
    """

    :param player_option:
    :return: None
    """
    signal.signal(signal.SIGINT, exit_handler)
    print(f'---> YOU ARE PLAYING: Rock-Paper-Scissors-Lizard-Spock <---')
    print(f'-> Pres CTRL+C to exit')
    cpu1 = get_cpu_option()
    player1 = player_option.lower().capitalize()

    if player_option == '':

        while True:
            player1 = input("Player's option: ").lower().capitalize()
            if player1 in win_options.keys():
                break
            else:
                msg = f'You picked "{player1}". Must pick from {str(win_options.keys())}'
                print(msg, end=" ")
                logging.warning(msg)

    elif player1 not in win_options.keys():
        while True:
            msg = f'You picked "{player1}". Must pick from: {str(win_options.keys())} '
            print(msg, end=" ")
            logging.warning(msg)

            player1 = input("Player's option: ").lower().capitalize()

            if player1 in win_options.keys():
                break

    print(f'---> Your choices <---')
    print(f'{"player1:":<10} {player1}')
    print(f'{"cpu1:":<10} {cpu1}')
    print(f'---> Who won? <---')

    decide_win(player1, cpu1)

    print(f'---> THE END <---')


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    main('')


