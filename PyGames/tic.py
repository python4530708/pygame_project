#: ------------------------------------------------ IMPORTS ------------------------------------------------
import signal
import logging

from PyGames.PyGames_functions import exit_handler

#: ----------------------------------------------- VARIABLES -----------------------------------------------
my_board = []
my_turn = 0
player = "O"
board_size = (3, 3)


#: ------------------------------------------------- CLASS -------------------------------------------------
class WrongMove(Exception):
    pass


#: ------------------------------------------------ METHODS ------------------------------------------------
def add_turn() -> None:
    global my_turn
    # my_turn = my_turn + 1
    my_turn += 1


def change_player(user: str) -> str:
    """
    switch between "X" and "O" to mark different players
    :param user: str
    :return: str
    """
    # if user == "O":
    #     user = "X"
    # else:
    #     user = "O"

    user = "X" if user == "O" else "O"

    return user


def check_last_turn() -> None:
    global my_turn
    global board_size

    moves = board_size[0] * board_size[1]

    if my_turn == moves:
        msg = 'This was last turn and it is DRAW'
        logging.info(msg)
        print(msg)
        print('---> The End<---')
        exit(0)


def check_win() -> bool:
    global player
    global my_board
    global board_size

    #: check rows for win
    for row in range(board_size[0]):
        win = True
        for column in range(board_size[1]):
            if my_board[row][column] != player:
                win = False
                break

        if win:
            return win

    #: check columns for win
    for column in range(board_size[1]):
        win = True
        for row in range(board_size[0]):
            if my_board[row][column] != player:
                win = False
                break

        if win:
            return win

    #: check diagonal for win
    win = True
    for row in range(board_size[0]):
        if my_board[row][row] != player:
            win = False
            break

    if win:
        return win

    win = True
    for row in range(board_size[0]):
        column = -1 - row
        if my_board[row][column] != player:
            win = False
            break

    if win:
        return win


def create_board() -> None:
    global my_board
    global board_size

    for line in range(board_size[0]):
        row = []
        for column in range(board_size[1]):
            row.append('-')

        my_board.append(row)


def make_move() -> None:
    global player
    global my_board

    print(f'make move by typing coordinate row 0-2, column 0-2 (minus between numbers): 1-2')

    while True:
        row, column = input(f'player "{player}": ').split("-", 1)
        row = int(row)
        column = int(column)

        try:
            if my_board[row][column] == '-':
                my_board[row][column] = player
                msg = f'Player "{player}" made move at row: {row}, column: {column}, at turn {my_turn +1}'
                logging.info(msg)
                break
            else:
                raise WrongMove('Other user already used this spot, pick other!')
        except WrongMove as ex:
            msg = f'-> {ex} -> Try again!'
            print(msg)
            logging.error(msg)
        except IndexError:
            msg = f'-> Out of range, Try again !'
            print(msg)
            logging.error(msg)

    add_turn()
    print_board()
    if check_win():
        msg = f'Player "{player}" won !'
        logging.info(msg)
        print(msg)
        print('---> The End <---')
        exit(0)
    check_last_turn()
    player = change_player(player)




def print_board() -> None:
    global my_board
    global my_turn
    global player

    print(f'---> Board, Turn {str(my_turn)}, Player "{player}" <---')
    print('  012')

    for row in range(len(my_board)):
        print(f'{str(row)} ', end='')

        for column in range(len(my_board[row])):
            print(my_board[row][column], end='')

        print('')

    print(f'---> Board End <---')


def main() -> None:
    signal.signal(signal.SIGINT, exit_handler)
    print(f'---> YOU ARE PLAYING: Tic-Tac-Toe <---')
    print(f'-> Pres CTRL+C to exit')

    create_board()
    print_board()

    while True:
        make_move()


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    main()
