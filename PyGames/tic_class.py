#: ------------------------------------------------ IMPORTS ------------------------------------------------
import signal
import logging
from os.path import basename

from pid import PidFile
from pid.base import PidFileAlreadyLockedError
from PyGames.PyGames_functions import exit_handler


#: ----------------------------------------------- VARIABLES -----------------------------------------------
#: ------------------------------------------------- CLASS -------------------------------------------------
class WrongMove(Exception):
    pass


class TicTacToe:
    def __init__(self, player: str = 'O') -> None:
        self.my_board = []
        self.my_turn = 0
        self.player = player
        self.board_size = 3

    def add_turn(self) -> None:
        # my_turn = my_turn + 1
        self.my_turn += 1

    def change_player(self) -> None:
        """
        switch between "X" and "O" to mark different players
        :param user: str
        :return: str
        """
        self.player = "X" if self.player == "O" else "O"

    def check_last_turn(self) -> None:
        moves = self.board_size * self.board_size

        if self.my_turn == moves:
            msg = 'This was last turn and it is DRAW'
            logging.info(msg)
            print(msg)
            print('---> The End<---')
            exit(0)

    def check_win(self) -> bool:
        #: check rows for win
        for row in range(self.board_size):
            win = True
            for column in range(self.board_size):
                if self.my_board[row][column] != self.player:
                    win = False
                    break

            if win:
                return win

        #: check columns for win
        for column in range(self.board_size):
            win = True
            for row in range(self.board_size):
                if self.my_board[row][column] != self.player:
                    win = False
                    break

            if win:
                return win

        #: check diagonal for win
        win = True
        for row in range(self.board_size):
            if self.my_board[row][row] != self.player:
                win = False
                break

        if win:
            return win

        win = True
        for row in range(self.board_size):
            column = -1 - row
            if self.my_board[row][column] != self.player:
                win = False
                break

        if win:
            return win

    def create_board(self) -> None:
        for line in range(self.board_size):
            row = []
            for column in range(self.board_size):
                row.append('-')

            self.my_board.append(row)

    def make_move(self) -> None:
        print(f'make move by typing coordinate row 0-2, column 0-2 (minus between numbers): 1-2')

        while True:
            row, column = input(f'player "{self.player}": ').split("-", 1)
            row = int(row)
            column = int(column)

            try:
                if self.my_board[row][column] == '-':
                    self.my_board[row][column] = self.player
                    msg = f'Player "{self.player}" made move at row: {row}, column: {column}, at turn {self.my_turn + 1}'
                    logging.info(msg)
                    break
                else:
                    raise WrongMove('Other user already used this spot, pick other!')
            except WrongMove as ex:
                msg = f'-> {ex} -> Try again!'
                print(msg)
                logging.error(msg)
            except IndexError:
                msg = f'-> Out of range, Try again !'
                print(msg)
                logging.error(msg)

        self.add_turn()
        self.print_board()
        if self.check_win():
            msg = f'Player "{self.player}" won !'
            logging.info(msg)
            print(msg)
            print('---> The End <---')
            exit(0)
        self.check_last_turn()
        self.change_player()

    def print_board(self) -> None:
        print(f'---> Board, Turn {str(self.my_turn)}, Player "{self.player}" <---')
        print('  012')

        for row in range(len(self.my_board)):
            print(f'{str(row)} ', end='')

            for column in range(len(self.my_board[row])):
                print(self.my_board[row][column], end='')

            print('')

        print(f'---> Board End <---')

    def start(self) -> None:
        print(f'---> YOU ARE PLAYING: Tic-Tac-Toe <---')
        print(f'-> Pres CTRL+C to exit')

        self.create_board()
        self.print_board()

        while True:
            self.make_move()


#: ------------------------------------------------ METHODS ------------------------------------------------
def main():
    try:
        script_name = basename(__file__)
        with PidFile(piddir='/home/wollwo/Projects/Python3/skolenie/pygame_project', pidname=f'{script_name}.pid'):
            signal.signal(signal.SIGINT, exit_handler)
            tic_tac_toe = TicTacToe()
            tic_tac_toe.start()

    except PidFileAlreadyLockedError:
        print('\n\nGame is already running')
        print('THE END')


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    main()
