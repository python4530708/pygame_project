import argparse
import platform
from PyGames import paper, tic, sub_popen, word

#: ------------------------------------------------ IMPORTS ------------------------------------------------
#: ----------------------------------------------- VARIABLES -----------------------------------------------
full_python_version = platform.python_version()
my_version = '1.00'


#: ------------------------------------------------- CLASS -------------------------------------------------
#: ------------------------------------------------ METHODS ------------------------------------------------
def main():
    parser = argparse.ArgumentParser(
        prog=f'{__package__}',
        description=f'You can pick between few games to play',
        epilog=f'This is running under python {full_python_version}'
    )

    exclusive_group = parser.add_mutually_exclusive_group(
        required=False
    )

    exclusive_group.add_argument(
        '--version', action='version', version=f'%(prog)s {my_version}'
    )

    exclusive_group.add_argument(
        '-p', '--paper',
        nargs='?',
        action='store',
        dest='paper',
        const='',
        type=str,
        help='Play "Rock-Paper-Scissors-Lizard-Spock"'
    )

    exclusive_group.add_argument(
        '-t', '--tic',
        action='store_true',
        dest='tic',
        default=False,
        help='Play "Tic-Tac-Toe"'
    )

    exclusive_group.add_argument(
        '-s', '--subprocess',
        nargs='?',
        action='store',
        dest='subprocess',
        const='notepad.exe',
        type=str,
        help='-s "application argument"'
    )

    exclusive_group.add_argument(
        '-w', '--find_word',
        nargs='+',
        action='store',
        dest='find_word',
        type=str,
        help='find word in log'
    )


    args = parser.parse_args()
    # print(args)   #: TESTING

    # if args.paper is not None:
    if args.paper is not None:
        paper.main(player_option=args.paper)
    elif args.tic:
        tic.main()
    elif args.subprocess is not None:
        sub_popen.main(process=args.subprocess)
    elif args.find_word is not None:
        print(args.find_word)
        if len(args.find_word) < 2:
            word.main(find_word=args.find_word[0])
        else:
            word.main(find_word=args.find_word[0], replace_word=args.find_word[1])

    else:
        parser.print_help()


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    main()
