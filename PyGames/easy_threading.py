#: ------------------------------------------------ IMPORTS ------------------------------------------------
from threading import Thread
from random import randrange
from time import sleep

#: ----------------------------------------------- VARIABLES -----------------------------------------------
outputs = []


#: ------------------------------------------------- CLASS -------------------------------------------------
#: ------------------------------------------------ METHODS ------------------------------------------------
def your_process(num: int) -> None:
    global outputs

    print(f'Process #{str(num)} Started')

    random_sleep = randrange(1, 10)

    sleep(random_sleep)

    print(f'Process #{str(num)} Stopped')
    outputs.append(f'Process #{str(num)} slept fro {random_sleep}')


def main(nth: int = 4) -> None:
    threads = []
    global outputs

    # Prepare threads
    print('>>> Preparing Threads')
    for process_index in range(1, nth):
        threads.append(Thread(target=your_process, args=(process_index,)))

    # Start Threads
    print('>>> Starting Threads')
    for thread in threads:
        thread.start()
    print('Loop of Starting ended here')

    # Join Threads
    for thread in threads:
        thread.join()

    print('>>> Here joined/exited threads')

    # Print Outputs
    for index, output in enumerate(outputs):
        print(f'output #{str(index)}: {output}')

    print('>>> THE END <<<')


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    main()
