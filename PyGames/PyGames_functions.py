#: ------------------------------------------------ IMPORTS ------------------------------------------------
#: ----------------------------------------------- VARIABLES -----------------------------------------------
#: ------------------------------------------------- CLASS -------------------------------------------------
#: ------------------------------------------------ METHODS ------------------------------------------------
def advanced_logthis(log_name: str = 'PyGame.log') -> object:
    from logging.handlers import RotatingFileHandler
    import logging

    #: initialization of logger
    advanced_logger = logging.getLogger('Pygame_logger')
    advanced_logger.setLevel(logging.DEBUG)

    #: initialization of formatter
    frm = ['%(asctime)s;%(name)s;%(levelname)s;%(message)s', '%Y.%m.%d %H:%M:%S']
    formatter = logging.Formatter(*frm)
    # formatter = logging.Formatter(
    #     '%(asctime)s;%(name)s;%(levelname)s;%(message)s',
    #     '%Y.%m.%d %H:%M:%S'
    # )

    #: initialization of handler
    handler = RotatingFileHandler(
        filename=log_name,
        mode='a',
        maxBytes=10000,
        backupCount=1,
        encoding='utf-8'
    )
    handler.setFormatter(formatter)
    advanced_logger.addHandler(handler)

    return advanced_logger


def exit_handler(signum, frame) -> None:
    """

    :param signum:
    :param frame:
    :return:
    """
    msg = f'You pressed CTRL+C, exiting this Awesome game!'
    print(f'\n{msg}')
    print('----> END <----')
    exit(1)

#: ------------------------------------------------- BODY --------------------------------------------------
adv_logger = advanced_logthis()
