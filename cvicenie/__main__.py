#: ------------------------------------------------ IMPORTS ------------------------------------------------
import signal
import argparse
import platform
import logging

from cvicenie import number
from pydantic import BaseSettings

#: ----------------------------------------------- VARIABLES -----------------------------------------------
my_version = "1.00"
full_python_version = platform.python_version()


#: ------------------------------------------------- CLASS -------------------------------------------------
class Settings(BaseSettings):
    min: int = 0
    max: int = 100
    turns: int = 10
    log_name: str = 'cvicenie.log'

    class Config:
        env_file = 'config.env'
        env_file_encoding = 'utf-8'


#: ------------------------------------------------ METHODS ------------------------------------------------
def calculator() -> None:
    pass


def exit_handler(signum, frame) -> None:
    """

    :param signum:
    :param frame:
    :return:
    """
    msg = f'You pressed CTRL+C, exiting this Awesome game!'
    print(f'\n{msg}')
    print('----> END <----')
    exit(1)


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    signal.signal(signal.SIGINT, exit_handler)

    sett = Settings()

    log_name = sett.log_name

    logging.basicConfig(
        filename=log_name,
        filemode='a',
        format='%(asctime)s;BASIC;%(levelname)s;%(message)s',
        datefmt='%Y.%m.%d %H:%M:%S',
        encoding='utf-8',
        level=logging.DEBUG
    )

    parser = argparse.ArgumentParser(
        prog=f'{__package__}',
        description=f'You can pick between "calculator" or "looking for number"',
        epilog=f'This is running under python {full_python_version}'
    )

    exclusive_group = parser.add_mutually_exclusive_group(
        required=False
    )

    exclusive_group.add_argument(
        '--version', action='version', version=f'%(prog)s {my_version}'
    )

    exclusive_group.add_argument(
        '-c', '--calculator',
        nargs='?',
        action='store',
        dest='calculator',
        const='',
        type=str,
        help='Run "calculator"'
    )

    # exclusive_group.add_argument(
    #     '-n', '--number',
    #     nargs=3,
    #     action='store',
    #     dest='number',
    #     default=(0, 10, 5),
    #     metavar=("MIN:int", "MAX:int", "TURNS:int"),
    #     help='Play "looking for number"'
    # )

    parser.add_argument(
        '-n', '--number',
        action='store_true',
        dest='number',
        default=False,
        help='Play "looking for number"'
    )

    parser.add_argument(
        '--min',
        nargs=1,
        action='store',
        dest='min',
        help='Play "looking for number"'
    )

    parser.add_argument(
        '--max',
        nargs=1,
        action='store',
        dest='max',
        help='Play "looking for number"'
    )

    parser.add_argument(
        '--turns',
        nargs=1,
        action='store',
        dest='turns',
        help='Play "looking for number"'
    )

    args = parser.parse_args()

    if args.calculator is not None:
        calculator()

    elif args.number:
        print(args)

        options = {}

        if args.min is not None:
            if args.min[0].isnumeric():
                min = int(args.min[0])
                options['min'] = min
            else:
                print("Argument MIN must be integer")
                parser.print_help()
                exit(1)

        if args.max is not None:
            if args.max[0].isnumeric():
                max = int(args.max[0])
                options['max'] = max
            else:
                print("Argument MAX must be integer")
                parser.print_help()
                exit(1)

        if args.turns is not None:
            if args.turns[0].isnumeric():
                turns = int(args.turns[0])
                options['turns'] = turns
            else:
                print("Argument TURNS must be integer")
                parser.print_help()
                exit(1)

        number.main(**options)

        # if args.number[0].isnumeric():
        #     min = int(args.number[0])
        # else:
        #     print("Argument MIN must be integer")
        #     parser.print_help()
        #     exit(1)

        # if args.number[1].isnumeric():
        #     max = int(args.number[1])
        # else:
        #     print("Argument MAX must be integer")
        #     parser.print_help()
        #     exit(1)

        # if args.number[2].isnumeric():
        #     turns = int(args.number[2])
        # else:
        #     print("Argument TURNS must be integer")
        #     parser.print_help()
        #     exit(1)

        # number.main(min, max, turns)

    else:
        parser.print_help()
        exit(1)