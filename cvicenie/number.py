#: ------------------------------------------------ IMPORTS ------------------------------------------------
import random
import logging

from cvicenie.__main__ import Settings

#: ----------------------------------------------- VARIABLES -----------------------------------------------
my_logger = 'there will be logger'
cpu = ""
player = ""
turn = 1
sett = Settings()

#: ------------------------------------------------- CLASS -------------------------------------------------
#: ------------------------------------------------ METHODS ------------------------------------------------
def check_last_turn(turn: int, turns: int) -> bool:
    if turn >= turns:
        return True
    return False


def compare_numbers(cpu: int, player: int) -> bool:
    if cpu > player:
        msg = f"Player's number {player} is smaller than CPU's"
        logging.info(msg)
        print(msg)
        return False
    elif cpu < player:
        msg = f"Player's number {player} is bigger than CPU's"
        logging.info(msg)
        print(msg)
        return False
    else:
        msg = f"You found the number {cpu}"
        logging.info(msg)
        print(msg)
        return True


def cpu_number(min: int = 0, max: int = 1000) -> int:
    return random.randint(min, max)


def read_number(min: int = 0, max: int = 1000) -> int:
    out = input(f"Player's option [{min}, {max}]: ")
    if out.isnumeric():
        out = int(out)
        if out >= min and out <= max:
            return int(out)
        else:
            return None
    else:
        return None


def main(min: int = None, max: int = None, turns: int = None) -> None:
    # print(cpu_number(min=0, max=10))
    global cpu
    global player
    global turn

    min = sett.min if min is None else min
    max = sett.max if max is None else max
    turns = sett.turns if turns is None else turns

    #: get CPU number
    cpu = cpu_number(min, max)
    while True:
        print(f'----> Looking for number, turn {turn}/{turns} <----')
        player = read_number(min, max)

        if player is None:
            msg = f'You picked wrong number {str(player)}, pick from [{str(min)}, {str(max)}]'
            logging.warning(msg)
            print(msg)
            continue
        else:
            win = compare_numbers(cpu, player)
            if win:
                msg = f"---->THE END<----"
                print(msg)
                exit(0)

        if check_last_turn(turn, turns):
            msg = f"this was last turn, turns: {turn}, CPU's number: {str(cpu)}"
            logging.info(msg)
            print(msg)
            print('---->THE END<----')
            exit(0)

        turn += 1

#: ------------------------------------------------- BODY --------------------------------------------------

