# Install libraries
```commandline
python -m pip install flask pydantic sqlmodel typing requests pid numpy matplotlib pandas
python3 -m pip install flask pydantic sqlmodel typing requests pid numpy matplotlib pandas
pip install flask pydantic sqlmodel typing requests pid numpy matplotlib pandas
pip3 install flask pydantic sqlmodel typing requests pid numpy matplotlib pandas
```

```commandline
git clone https://wollwo:glpat-XzsX83z3kiLMeFpmWJGw@gitlab.com/python4530708/pygame_project.git
```

```commandline
import requests
r = requests.get('http://localhost:8080/')
print(r)
print(r.text)
print(r.content)
print(r.headers)
print(r.status_code)
```






## Uloha 01
- [ ] Spusti program ako modul (python -m)
- [ ] očakávaj argumenty, ak argument nebol podaný print help, exit 1
- [ ] musí obsahovať logovanie error-ov, a všetkých pokusov, na konci zaloguje aj to hľadané číslo
- [ ] program je hra "hádaj číslo", kde bude vygenerované náhodne číslo a ty ho hľadáš, program ti povie či je hľadané číslo menšie alebo väčšie ako to čo si zadal
- [ ] očakávaj input z klavesnice a ak nebolo zadané číslo, zopakuj možnosť pre hráča
- [ ] skontroluj či input je integer
- [ ] cez argumenty zadáme koľko pokusov môžeme mať a z akého intervalu sa vytvorí hľadané číslo, default nekonečno (pokiaľ nerobíš optional config)
- [ ] OPTIONAL: ak nebudu čísla intervalu alebo počet pokusov zadaný cez argumenty, použiť config kde bude defaultna hodnota 
- [ ] OPTIONAL: CTRL+C exit handler
- [ ] OPTIONAL: do it in class

## Uloha 02
- [ ] Spusti program ako modul (python -m)
- [ ] očakávaj argumenty, ak argument nebol podaný print help, exit 1
- [ ] musí obsahovať logovanie error-ov, a všetkých pokusov, na konci zaloguje aj to hľadané číslo
- [ ] program je kalkulačka a okrem základných operácií [+-*/] bude mať aj druhú mocninu a odmocninu (math knižnica)
- [ ] Buď to spustíš cez argumenty, alebo spustíš textové gui ktoré sa vypne zadaním voľby "q"











# Ukazky
```commandline
In [5]: math.ceil(10.1)
Out[5]: 11

In [7]: math.fabs(-10)
Out[7]: 10.0

In [9]: math.factorial(3)
Out[9]: 6

In [11]: math.floor(10.1)
Out[11]: 10

In [13]: math.sqrt(4)
Out[13]: 2.0

In [14]: math.pow(3, 2)
Out[14]: 9.0


In [23]: mydata = {
    ...:   'cars': ['BMW', 'VOLVO', 'FORD'],
    ...:   'passings': [3, 7, 2]
    ...:   }

In [24]: m = pandas.DataFrame(mydata)

In [25]: print(m)
    cars  passings
0    BMW         3
1  VOLVO         7
2   FORD         2



n [28]: myvar = pd.Series(a, index = ["x", "y", "z"])

In [29]: print(myvar)
x    1
y    7
z    2
dtype: int64

In [30]:




In [30]: import pandas as pd
    ...: 
    ...: calories = {"day1": 420, "day2": 380, "day3": 390}
    ...: 
    ...: myvar = pd.Series(calories, index = ["day1", "day2"])
    ...: 
    ...: print(myvar)
day1    420
day2    380
dtype: int64

In [40]: q = pd.read_csv('/home/wollwo/Projects/Python3/skolenie/pygame_project/out_PyGame.log', sep=";")

In [41]: q
Out[41]: 
                  date         logger log_level                                            message
0  2023.02.28 14:19:56  Pygame_logger      INFO  Looking for word "Potter", found it in 25 line...
1  2023.02.28 14:19:56  Pygame_logger      INFO  Looking for word "Potter", found it in 25 line...

In [42]: q.to_json()
Out[42]: '{"date":{"0":"2023.02.28 14:19:56","1":"2023.02.28 14:19:56"},"logger":{"0":"Pygame_logger","1":"Pygame_logger"},"log_level":{"0":"INFO","1":"INFO"},"message":{"0":"Looking for word \\"Potter\\", found it in 25 lines [str.find()]","1":"Looking for word \\"Potter\\", found it in 25 lines [re.search()]"}}'

In [43]: w = q.to_json()

In [44]: w
Out[44]: '{"date":{"0":"2023.02.28 14:19:56","1":"2023.02.28 14:19:56"},"logger":{"0":"Pygame_logger","1":"Pygame_logger"},"log_level":{"0":"INFO","1":"INFO"},"message":{"0":"Looking for word \\"Potter\\", found it in 25 lines [str.find()]","1":"Looking for word \\"Potter\\", found it in 25 lines [re.search()]"}}'

In [47]: pd.read_json(w)
Out[47]: 
                 date         logger log_level                                            message
0 2023-02-28 14:19:56  Pygame_logger      INFO  Looking for word "Potter", found it in 25 line...
1 2023-02-28 14:19:56  Pygame_logger      INFO  Looking for word "Potter", found it in 25 line...




source ./Virtualenvs/beeware-venv/bin/activate
In [2]: import matplotlib.pyplot as plt

In [3]: import numpy as np

In [4]: x = np.array([0, 6])

In [5]: y = np.array([0, 250])

In [6]: plt.plot(x, y)
Out[6]: [<matplotlib.lines.Line2D at 0x7f40a6bcd780>]

In [7]: plt.show()

In [8]:


source ./Virtualenvs/beeware-venv/bin/deactivate


In [53]: import matplotlib.pyplot as plt
In [54]: import numpy as np

In [55]: x = np.array([0, 6, 10, 20])

In [56]: y = np.array([0, 250, 100, 110])



In [63]: x = np.array(range(1, 10))

In [64]: y = np.array(x**2)

In [60]: plt.plot(x, y)
Out[60]: [<matplotlib.lines.Line2D at 0x7f9946f97e50>]

In [61]: plt.savefig("/tmp/graf.jpg")

In [65]: plt.plot(x, y)
Out[65]: [<matplotlib.lines.Line2D at 0x7f9956051f90>]

In [66]: plt.show()
```