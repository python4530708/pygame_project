#: ------------------------------------------------ IMPORTS ------------------------------------------------
from pydantic import BaseModel
from dataclasses import dataclass

#: ----------------------------------------------- VARIABLES -----------------------------------------------
#: ------------------------------------------------- CLASS -------------------------------------------------
class LogLine(BaseModel):
    date: str
    logger: str
    log_level: str
    msg: str


@dataclass
class TestClass:
    a: int = 1
    b: str = 'a'


#: ------------------------------------------------ METHODS ------------------------------------------------
def lst(j, h, b) -> list:
    """

    :param j:
    :param h:
    :param b:
    :return: list, [jablka, hrusky, banany]
    """
    j = 10
    h = 5
    b = 1
    return [j, h, b]


def main():
    with open('PyGame.log', 'r', encoding='utf-8') as file:
        lines = file.readlines()

    log_lines = []
    for line in lines:
        arg = line.split(';', 4)
        kwrgs = {
            'date': arg[0],
            'logger': arg[1],
            'log_level': arg[2],
            'msg': arg[3]
        }
        # l = LogLine(date=arg[0], logger=arg[1], log_level=arg[2], msg=arg[3])
        l = LogLine(**kwrgs)
        log_lines.append(l)


    print(log_lines[0])
    print(type(log_lines[0]))


#: ------------------------------------------------- BODY --------------------------------------------------
if __name__ == '__main__':
    main()
    a = lst()